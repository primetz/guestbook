<?php

namespace App\Service\ImageOptimizer;

interface ImageOptimizerInterface
{
    public function resize(string $filename): void;
}
