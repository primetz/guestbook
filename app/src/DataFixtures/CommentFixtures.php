<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $comment1 = new Comment();
        $comment1->setConference($this->getReference(ConferenceFixtures::AMSTERDAM_REFERENCE))
            ->setAuthor('Fabien')
            ->setEmail('fabien@example.com')
            ->setText('This was a great conference.')
            ->setState('published');
        $manager->persist($comment1);

        $comment2 = new Comment();
        $comment2->setConference($this->getReference(ConferenceFixtures::AMSTERDAM_REFERENCE))
            ->setAuthor('Lucas')
            ->setEmail('lucas@example.com')
            ->setText('I think this one is going to be moderated.');
        $manager->persist($comment2);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            ConferenceFixtures::class,
        ];
    }
}
