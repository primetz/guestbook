<?php

namespace App\Notification;

use App\Entity\Comment;
use Symfony\Component\Notifier\Bridge\Slack\Block\SlackActionsBlock;
use Symfony\Component\Notifier\Bridge\Slack\Block\SlackDividerBlock;
use Symfony\Component\Notifier\Bridge\Slack\Block\SlackImageBlockElement;
use Symfony\Component\Notifier\Bridge\Slack\Block\SlackSectionBlock;
use Symfony\Component\Notifier\Bridge\Slack\SlackOptions;
use Symfony\Component\Notifier\Bridge\Telegram\Reply\Markup\Button\InlineKeyboardButton;
use Symfony\Component\Notifier\Bridge\Telegram\Reply\Markup\InlineKeyboardMarkup;
use Symfony\Component\Notifier\Bridge\Telegram\TelegramOptions;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Notification\ChatNotificationInterface;
use Symfony\Component\Notifier\Notification\EmailNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

class CommentReviewNotification extends Notification implements EmailNotificationInterface, ChatNotificationInterface
{
    private const SUPPORTED_CHAT_TRANSPORTS = ['slack', 'telegram'];

    public function __construct(
        private readonly Comment $comment,
        private readonly string  $reviewUrl,
        private readonly string $photoDirUrl,
    )
    {
        parent::__construct('New comment posted');
    }

    public function asEmailMessage(EmailRecipientInterface $recipient, string $transport = null): ?EmailMessage
    {
        $message = EmailMessage::fromNotification($this, $recipient);
        $message->getMessage()
            ->htmlTemplate('emails/comment_notification.html.twig')
            ->context(['comment' => $this->comment]);

        return $message;
    }

    public function getChannels(RecipientInterface $recipient): array
    {
        if (preg_match('{\b(slack)\b}i', $this->comment->getText())) {

            return ['chat/slack'];
        }

        if (preg_match('{\b(email)\b}i', $this->comment->getText())) {

            $this->importance(Notification::IMPORTANCE_LOW);

            return ['email'];
        }

        return ['chat/telegram'];
    }

    public function asChatMessage(RecipientInterface $recipient, string $transport = null): ?ChatMessage
    {
        if (!in_array($transport, self::SUPPORTED_CHAT_TRANSPORTS, true)) {
            return null;
        }

        $message = ChatMessage::fromNotification($this);

        $subject = match ($transport) {
            'slack' => $this->getSubject(),
            'telegram' => $this->telegramHtmlMarkup(),
        };

        $options = $this->chatMessageOptions($transport);

        if ($this->comment->getPhotoFilename()) {

            $photoUrl = $this->photoDirUrl . $this->comment->getPhotoFilename();

            match ($transport) {
                'telegram' => $options->photo($photoUrl),
                'slack' => $options->block((new SlackSectionBlock())->text('Photo')->accessory(new SlackImageBlockElement($photoUrl, 'Photo')))
            };
        }

        return $message->subject($subject)->options($options);
    }

    private function chatMessageOptions(string $transport): SlackOptions|TelegramOptions
    {
        return match ($transport) {
            'slack' => (new SlackOptions())
                            ->iconEmoji('ibook')
                            ->username('Gbook')
                            ->block((new SlackSectionBlock())
                                ->text($this->getSubject())
                            )
                            ->block(new SlackDividerBlock())
                            ->block((new SlackSectionBlock())
                                ->text(sprintf('%s (%s) says: %s', $this->comment->getAuthor(), $this->comment->getEmail(), $this->comment->getText()))
                            )
                            ->block((new SlackActionsBlock())
                                ->button('Accept', $this->reviewUrl, 'primary')
                                ->button('Reject', $this->reviewUrl . '?reject=1', 'danger')
                            ),
            default => (new TelegramOptions())
                            ->parseMode(TelegramOptions::PARSE_MODE_HTML)
                            ->disableWebPagePreview(true)
                            ->replyMarkup((new InlineKeyboardMarkup())
                                ->inlineKeyboard([
                                    (new InlineKeyboardButton('Accept'))
                                        ->url($this->reviewUrl),
                                    (new InlineKeyboardButton('Reject'))
                                        ->url($this->reviewUrl . '?reject=1')
                                ])
                            )
        };
    }

    private function telegramHtmlMarkup(): string
    {
        return sprintf(
            '<b><u>Author:</u></b> %s' . PHP_EOL .
            '<b><u>Email:</u></b> %s' . PHP_EOL .
            '<blockquote>%s</blockquote>',
            $this->comment->getAuthor(),
            $this->comment->getEmail(),
            $this->comment->getText()
        );
    }
}
