vcl 4.0;

import directors;

backend default none;

backend gbook {
    .host = "gbook-nginx";
    .port = "80";
}

sub vcl_init {
    new vdir = directors.round_robin();
    vdir.add_backend(gbook);
}

sub vcl_deliver {
    unset resp.http.Age;
    unset resp.http.Via;
    unset resp.http.Server;
    unset resp.http.X-Varnish;
}

sub vcl_recv {
    set req.backend_hint = vdir.backend();
    set req.http.Surrogate-Capability = "abc=ESI/1.0";

    if (req.method == "PURGE") {
        if (req.http.x-purge-token != "PURGE_NOW") {
            return(synth(405));
        }
        return (purge);
    }
}

sub vcl_backend_response {
    if (beresp.http.Surrogate-Control ~ "ESI/1.0") {
        unset beresp.http.Surrogate-Control;
        set beresp.do_esi = true;
    }
}
