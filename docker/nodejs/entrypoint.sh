#!/bin/bash

npm install && cd /app/spa && npm install;

if [ "${APP_ENV}" = dev ]; then
    npm run watch \
    && cd /app/spa \
    && npm run watch;
    else \
      npm run build \
      && cd /app/spa \
      && npm run build;
fi
